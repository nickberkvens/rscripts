library(data.table)
library(dplyr)

D_EVENT = fread(file="C:/Users/nickberkvens/Documents/R_Scripts/Extracted_files/D_EVENT.csv", sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
D_TYPE = fread(file="C:/Users/nickberkvens/Documents/R_Scripts/Extracted_files/D_TYPE.csv", sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
D_THEMA = fread(file="C:/Users/nickberkvens/Documents/R_Scripts/Extracted_files/D_THEMA.csv", sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
D_EVENTTHEMES = fread(file="C:/Users/nickberkvens/Documents/R_Scripts/Extracted_files/D_EVENTTHEMES.csv", sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
D_EVENTTYPES = fread(file="C:/Users/nickberkvens/Documents/R_Scripts/Extracted_files/D_EVENTTYPES.csv", sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")

names(D_EVENTTHEMES)[1] = "DEVE_BKEY"
names(D_EVENTTYPES)[1] = "DEVE_BKEY"
names(D_TYPE)[2] = "categorization_TYPE"
names(D_TYPE)[1] = "TypeID"
names(D_THEMA)[2] = "categorization_THEMA"
names(D_THEMA)[1] = "ThemeID"

D_EVENT$DEVE_STARTDATE = as.Date(D_EVENT$DEVE_STARTDATE)
D_EVENT = D_EVENT[D_EVENT$DEVE_STARTDATE >= "2018-01-01",]
D_EVENT = D_EVENT[D_EVENT$DEVE_STARTDATE < "2019-01-01",]

D_EVENT = left_join(D_EVENT,D_EVENTTHEMES, by = "DEVE_BKEY") 
D_EVENT = left_join(D_EVENT,D_THEMA[,c('ThemeID','categorization_THEMA')], by = "ThemeID")
D_EVENT = left_join(D_EVENT,D_EVENTTYPES, by = "DEVE_BKEY") 
D_EVENT = left_join(D_EVENT,D_TYPE[,c('TypeID','categorization_TYPE')], by = "TypeID")

D_EVENT = D_EVENT[D_EVENT$DEVE_DELETED == FALSE,]

D_EVENT = D_EVENT[,c("DEVE_SKEY","DEVE_BKEY","DEVE_LABEL","DEVE_SHORT_DESC","DEVE_AVAILABLE_FROM","DEVE_PRIVATE",
                     "DEVE_CALENDAR_SUMMARY","DEVE_LONGITUDE","DEVE_LATITUDE","DEVE_PERFORMANCE","DEVE_DEEPLINK","DEVE_THUMBNAIL",
                     "DEVE_IS_PRODUCTION","DEVE_LONG_DESCRIPTION","DEVE_PRICE","DEVE_PRICEDESCRIPTION","DEVE_PERMANENT",
                     "DEVE_TRANSLATED_DE","DEVE_TRANSLATED_EN","DEVE_TRANSLATED_FR","DEVE_STARTDATE","DEVE_ENDDATE",
                     "DEVE_CURRENT","DEVE_CDBID","DEVE_AGEFROM","DEVE_TAGS","DEVE_CITY","DEVE_WORKFLOWSTATUS","DEVE_PROVINCE",
                     "DEVE_URL","DEVE_HOOFDGEMEENTE","DEVE_PRIVE","DEVE_TICKETING_URL","DEVE_URL_RESERVATION",
                     "DEVE_LOCATION","DEVE_ORGANISER","DEVE_PUBLISHED","DEVE_RESERVATIEPERIODE","DEVE_FACEBOOK_URL",
                     "DEVE_ZIPCODE","DEVE_COUNTRY","DEVE_CREATEDON","DEVE_AVAILABLETO","DEVE_HOUSENR","DEVE_STREET",
                     "DEVE_LANGUAGE_ID","DEVE_TRANSLATED_NL","ThemeID","categorization_THEMA","TypeID","categorization_TYPE")]

fwrite(D_EVENT, file="C:/Users/nickberkvens/Desktop/UGent_Thesis/anonym_Events_2018_UGENT.csv", row.names=F)



ACTIVITIES = fread(file="C:/Users/nickberkvens/Desktop/UGent_Thesis/export_activities_2018_oct_to_dec_UGENT.csv", sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
names(ACTIVITIES)

ACTIVITIES = ACTIVITIES[, c("ACTIVITYENTITY_ID",
                            # "ACTIVITYENTITY_CONSUMPTIONTYPE",
                            "ACTIVITYENTITY_CONTENTTYPE",
                            "ACTIVITYENTITY_CREATIONDATE",
                            "ACTIVITYENTITY_DELETED",
                            "ACTIVITYENTITY_EVENTLOCATIONLAT",
                            "ACTIVITYENTITY_EVENTLOCATIONLON",
                            "ACTIVITYENTITY_KANSENSTATUUT",
                            "ACTIVITYENTITY_NODETITLE",
                            "ACTIVITYENTITY_ORGANISER",
                            "ACTIVITYENTITY_POINTS",
                            # "ACTIVITYENTITY_PRIV",
                            "ACTIVITYENTITY_USERHOMECITY",
                            "ACTIVITYENTITY_CONSUMPTIONTYPE_NAME",
                            "DALIUSER_ID",
                            "DALIUSER_DTYPE",
                            "DALIUSER_CREATIONDATE",
                            "DALIUSER_LASTLOGINDATE",
                            "DALIUSER_STATUS",
                            "DALIUSER_CREATEDBY_ID",
                            "DALIUSER_CITY",
                            "DALIUSER_COUNTRY",
                            "DALIUSER_DATEOFBIRTH",
                            "DALIUSER_HOMELAT",
                            "DALIUSER_HOMELON",
                            "DALIUSER_LASTACTIVITYDATE",
                            # "DALIUSER_LOCATIONLAT",
                            # "DALIUSER_LOCATIONLON",
                            "DALIUSER_NUMBEROFACTIVEACTIVITIES",
                            "DALIUSER_NUMBEROFACTIVITIES",
                            "DALIUSER_NUMBEROFATTENDS",
                            "DALIUSER_NUMBEROFLIKES",
                            "DALIUSER_NUMBEROFSHARESFACEBOOK",
                            "DALIUSER_NUMBEROFSHARESTWITTER",
                            "DALIUSER_OPENID",
                            "DALIUSER_POINTS",
                            "DALIUSER_ZIP",
                            "DALIUSER_REGISTRATIONLANGUAGE",
                            "DALIUSER_PREFERREDLANGUAGE",
                            "DALIUSER_LIGHT",
                            "DALIUSER_OPTINSERVICEMAILS",
                            "DALIUSER_OPTINSERVICEMAILSDATE",
                            "DALIUSER_OPTININFOMAILS",
                            "DALIUSER_OPTININFOMAILSDATE",
                            "DALIUSER_OPTINMILESTONEMAILS",
                            "DALIUSER_OPTINMILESTONEMAILSDATE",
                            "DALIUSER_OPTINSMS",
                            "DALIUSER_OPTINSMSDATE",
                            "DALIUSER_OPTINPOST",
                            "DALIUSER_OPTINPOSTDATE",
                            "DALIUSER_BALIEMEDEWERKERVERKLARINGOPEER",
                            "PASSHOLDER_ID",
                            "PASSHOLDER_ACTIVATIONVERIFICATIONDATE",
                            "PASSHOLDER_CITY",
                            "PASSHOLDER_CREATIONDATE",
                            "PASSHOLDER_DATEOFBIRTH",
                            "PASSHOLDER_GENDER",
                            "PASSHOLDER_KANSENSTATUUT",
                            "PASSHOLDER_LASTCHECKINDATE",
                            # "PASSHOLDER_MOREINFO",
                            "PASSHOLDER_NUMBEROFCHECKINS",
                            "PASSHOLDER_POSTALCODE",
                            "PASSHOLDER_REGISTRATIONBALIE_ID",
                            "PASSHOLDER_WELCOMEPACKAGE_ID",
                            "PASSHOLDERCARDSYSTEMLINK_ID",
                            "PASSHOLDERCARDSYSTEMLINK_STATUS",
                            "PASSHOLDERCARDSYSTEMLINK_CARDSYSTEM_ID",
                            # "CARD_ID",
                            "CARD_DATEACTIVATED",
                            "CARD_DATEBLOCKED",
                            "CARD_DATEDELETED",
                            "CARD_KANSENPAS",
                            # "CARD_BALIE_ID",
                            "PASSHOLDER_CARDSYSTEM_NAME",
                            "CONTENT_ID",
                            "CONTENT_CREATIONDATE",
                            "CONTENT_MODIFICATIONDATE",
                            "CONTENT_SUMMARY",
                            "CONTENT_CDBID",
                            "CONTENT_CITY",
                            "CONTENT_LOCATIONNAME",
                            "CONTENT_NUMBEROFPOINTS",
                            "CONTENT_PRICE",
                            "CONTENT_CARDSYSTEM_NAME",
                            "CONTENT_CARDSYSTEM_MARKER")]
ACTIVITIES$DALIUSER_HOMELAT = ifelse(!is.na(ACTIVITIES$DALIUSER_HOMELAT), "bestaat",ACTIVITIES$DALIUSER_HOMELAT)
ACTIVITIES$DALIUSER_HOMELON = ifelse(!is.na(ACTIVITIES$DALIUSER_HOMELON), "bestaat",ACTIVITIES$DALIUSER_HOMELON)
ACTIVITIES$PASSHOLDER_ID = ACTIVITIES$PASSHOLDER_ID*7
ACTIVITIES$DALIUSER_ID = ACTIVITIES$DALIUSER_ID*13
ACTIVITIES$ACTIVITYENTITY_ID = ACTIVITIES$ACTIVITYENTITY_ID*3

fwrite(ACTIVITIES, file="C:/Users/nickberkvens/Desktop/UGent_Thesis/anonym_export_activities_2018_oct_to_dec_UGENT.csv", row.names=F)

