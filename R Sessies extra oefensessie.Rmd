---
title: "R Sessies extra oefensessie"
output: html_document
---
bruikbare cheatsheets:  
* Cheatsheet Base R  
* CheatsheetShort refcard R  
* Cheatsheet rstudio-ide R  



```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## herhalingsoefeningen

```{r oefeningen, echo = T, results = 'hide' }
library(data.table) # activeer package en zijn bijhorende functies

D_EVENT = fread(file="C:/Users/nickberkvens/Desktop/D_EVENT_sessie2_short.csv", sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")

# hoeveel evenementen vinden plaats in Brussel
conditie = D_EVENT$DEVE_CITY == "Brussel"
subset = D_EVENT[conditie,]
berekening = nrow(subset)
berekening
# in 1 lijn:
nrow(D_EVENT[D_EVENT$DEVE_CITY == "Brussel",])
  
  
# 1 welke evenement staat op rij 5027 in de tabel?

# 2 in welke stad neemt het evenement in rij 7027 plaats?

# 3 hoeveel unieke locaties zijn er waarop evenementen georganiseerd worden?

# 4 hoeveel evenementen worden georganiseerd in Brussel?

# 5 wat is de gemiddelde prijs van de evenementen die in Brussel plaatsvinden?

# 6 hoeveel evenementen in Oostende hebben geen bijhorende afbeelding?

# 7 hoeveel unieke locaties telt Brussel waarop evenementen georganiseerd worden?

# 8 in hoeveel provincies worden permanente evenementen georganiseerd?

# 9 wat is de gemiddelde prijs van evenementen in Oostende die geen bijhorende afbeelding hebben?

# 10 in hoeveel hoofdgemeenten worden evenementen georganiseerd met een leeftijdsrestrictie vanaf +50 jaar?

# 11 wat is de gemiddelde prijs van evenementen in Brussel die geen bijhorende afbeelding hebben?

# 12 hoeveel organisatoren organiseren evenementen met een leeftijdsrestrictie vanaf +55 jaar?

# 13 wat is de gemiddelde prijs van evenementen in Brussel met een bijhorende afbeelding en duurder dan 10 euro?

# 14 in hoeveel provincies worden evenementen georganiseerd die duurder zijn dan 1000 euro?

# 15 wat zijn de min en max prijzen van evenementen in Brussel met een bijhorende afbeelding en duurder dan 10 euro?

# 16 wat is de gemiddelde prijs van evenementen met een leeftijdsrestrictie vanaf +45 jaar

# 17 in hoeveel gemeenten worden evenementen georganiseerd die in hoofdgemeente Gent plaatsvallen?

# 18 corrigeer de "foutive" label "Sint-Amandsberg (Gent)" naar "Sint-Amandsberg"

# 19 wat is de gemiddelde prijs van evenementen met een leeftijdsrestrictie vanaf +45 jaar in Provincie Antwerpen

# 20 verander de ontbrekende waarden bij DEVE_AGEFROM in de gemiddelde leeftijdsbeperking

# 21 maak volgende leeftijdsbeperking-groepen c(0,5,10,20,30,40,65)?

# 22 er zijn 20 evenementen zonder categorie terwijl net alle ontbrekende waarden werden opgekuist?

# 23 wat is de gemiddelde prijs van de evenementen per leeftijdscategorie?

# 24 categoriseer de evenementen in 4 categorien op basis van hun long-lat gegevens, hoeveel evenementen per categorie
# A: LON <= 4.373 en LAT <= 50.98 _ B: LON <= 4.373 en > 50.98 _ C: LON > 4.373 en <= 50.98 _ D: LON > 4.373 en > 50.98 

# 25 wat is de gemiddelde prijs van de evenementen in categorie A

# 26 wat is de gemiddelde prijs van de evenementen in categoriŽn A en C

# 27 hoeveel evenementen in gent hebben missende lon-lat coordinaten?

# 28 verander de missende lon - lat gegevens van de evenementen die plaats vinden in Gent naar de middelpuntcoordinaten van de stad = 3.7167 , 51.05
















# 1 
D_EVENT[5027,]

Julie = D_EVENT[5027,]
Julie = D_EVENT[c(5027,5050),]
Julie = D_EVENT[c(5027:5050),]

# 2 
D_EVENT$DEVE_CITY[5027] 

Julie = D_EVENT[,"DEVE_CITY"]
Julie = D_EVENT[5027,"DEVE_CITY"]
Julie = D_EVENT$DEVE_CITY[5027]
Julie = D_EVENT[5027,]$DEVE_CITY

# 3
length(unique(D_EVENT$DEVE_LOCATION))

locations = D_EVENT$DEVE_LOCATION
locations = unique(locations)
locations = length(locations)

# 4 
nrow(D_EVENT[D_EVENT$DEVE_CITY == "Brussel",])

conditie = D_EVENT$DEVE_CITY == "Brussel" 
subset = D_EVENT[conditie,]
subset = D_EVENT[D_EVENT$DEVE_CITY == "Brussel",]
berekening = nrow(subset)
berekening = nrow(D_EVENT[D_EVENT$DEVE_CITY == "Brussel",])

# 5 
mean(D_EVENT$DEVE_PRICE[D_EVENT$DEVE_CITY == "Brussel"], na.rm = TRUE)

conditie = D_EVENT$DEVE_CITY == "Brussel" 
subset = D_EVENT[conditie,]
subset = mean(subset$DEVE_PRICE, na.rm=TRUE)
subset
# summary(subset$DEVE_PRICE)

conditie = D_EVENT$DEVE_CITY == "Brussel" 
subset = mean(D_EVENT[conditie,"DEVE_PRICE"], na.rm=TRUE)
subset = mean(D_EVENT$DEVE_PRICE[conditie], na.rm=TRUE)

# 6 
nrow(D_EVENT[D_EVENT$DEVE_THUMBNAIL == 'FALSE' & D_EVENT$DEVE_CITY == 'Oostende',])

conditie_1 = D_EVENT$DEVE_CITY == "Oostende" 
conditie_2 = D_EVENT$DEVE_THUMBNAIL == "FALSE"
subset = D_EVENT[conditie_1 & conditie_2,]
berekening = nrow(subset)

conditie = D_EVENT$DEVE_CITY == "Oostende" & D_EVENT$DEVE_THUMBNAIL == "FALSE"
subset = D_EVENT[conditie,]
berekening = nrow(subset)

# 7 
length(unique(D_EVENT$DEVE_LOCATION[D_EVENT$DEVE_CITY == 'Brussel']))

conditie = D_EVENT$DEVE_CITY=='Brussel'
subset = D_EVENT[conditie,]
kolom = subset$DEVE_LOCATION
kolom = unique(kolom)
length(kolom)

# 8
length(unique(D_EVENT$DEVE_PROVINCE[D_EVENT$DEVE_PERMANENT == 1]))

# 9 
mean(D_EVENT$DEVE_PRICE[D_EVENT$DEVE_THUMBNAIL == 'FALSE' & D_EVENT$DEVE_CITY == 'Oostende'])

conditie = D_EVENT$DEVE_CITY == 'Oostende'
subset = D_EVENT[conditie,]
conditie = subset$DEVE_THUMBNAIL == FALSE
subset = subset[conditie,]
kolom = subset$DEVE_PRICE
mean(kolom)



# 10
length(unique(D_EVENT$DEVE_HOOFDGEMEENTE[D_EVENT$DEVE_AGEFROM > 50 & !is.na(D_EVENT$DEVE_AGEFROM)])) # 14
length(unique(D_EVENT$DEVE_HOOFDGEMEENTE[D_EVENT$DEVE_AGEFROM > 50])) # 15

# 11
mean(D_EVENT$DEVE_PRICE[D_EVENT$DEVE_THUMBNAIL == 'FALSE' & D_EVENT$DEVE_CITY == 'Brussel'], na.rm = TRUE)

# 12
length(unique(D_EVENT$DEVE_ORGANISER[D_EVENT$DEVE_AGEFROM > 55 & !is.na(D_EVENT$DEVE_AGEFROM)])) # 6
length(unique(D_EVENT$DEVE_ORGANISER[D_EVENT$DEVE_AGEFROM > 55])) # 7

# 13 
mean(D_EVENT$DEVE_PRICE[D_EVENT$DEVE_THUMBNAIL == 'TRUE' & D_EVENT$DEVE_CITY == 'Brussel' & D_EVENT$DEVE_PRICE > 10], na.rm = TRUE)

# 14
length(unique(D_EVENT$DEVE_PROVINCE[D_EVENT$DEVE_PRICE > 1000 & !is.na(D_EVENT$DEVE_PRICE)])) # 4
length(unique(D_EVENT$DEVE_PROVINCE[D_EVENT$DEVE_PRICE > 1000 ])) # 5

# 15 
range(D_EVENT$DEVE_PRICE[D_EVENT$DEVE_THUMBNAIL == 'TRUE' & D_EVENT$DEVE_CITY == 'Brussel' & D_EVENT$DEVE_PRICE > 10], na.rm = TRUE)

# 16
mean(D_EVENT$DEVE_PRICE[D_EVENT$DEVE_AGEFROM > 45], na.rm= TRUE) # 14

# 17
length(unique(D_EVENT$DEVE_ZIPCODE[D_EVENT$DEVE_HOOFDGEMEENTE == 'Gent']))  # 3 
length(unique(D_EVENT$DEVE_CITY[D_EVENT$DEVE_HOOFDGEMEENTE == 'Gent'])) # 4

# 18
D_EVENT$DEVE_CITY = ifelse(D_EVENT$DEVE_CITY == "Sint-Amandsberg (Gent)", "Sint-Amandsberg" , D_EVENT$DEVE_CITY)
D_EVENT$DEVE_CITY = factor(D_EVENT$DEVE_CITY)
length(unique(D_EVENT$DEVE_CITY[D_EVENT$DEVE_HOOFDGEMEENTE == 'Gent'])) # 4
unique(D_EVENT$DEVE_CITY[D_EVENT$DEVE_HOOFDGEMEENTE == 'Gent'])

D_EVENT = fread(file="C:/Users/nickberkvens/Desktop/D_EVENT_sessie2_short.csv", sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
D_EVENT$DEVE_CITY = ifelse(D_EVENT$DEVE_CITY == "Sint-Amandsberg (Gent)", "Sint-Amandsberg" , as.character(D_EVENT$DEVE_CITY))
D_EVENT$DEVE_CITY = factor(D_EVENT$DEVE_CITY)
length(unique(D_EVENT$DEVE_CITY[D_EVENT$DEVE_HOOFDGEMEENTE == 'Gent'])) # 4
unique(D_EVENT$DEVE_CITY[D_EVENT$DEVE_HOOFDGEMEENTE == 'Gent'])

# 19
mean(D_EVENT$DEVE_PRICE[D_EVENT$DEVE_AGEFROM > 45 & !is.na(D_EVENT$DEVE_AGEFROM) & D_EVENT$DEVE_PROVINCE == "Provincie Antwerpen" ], na.rm= TRUE)

# 20
summary(D_EVENT$DEVE_AGEFROM)
D_EVENT$DEVE_AGEFROM[is.na(D_EVENT$DEVE_AGEFROM)] = mean(D_EVENT$DEVE_AGEFROM, na.rm = TRUE)
summary(D_EVENT$DEVE_AGEFROM)

# 21 
D_EVENT$CAT_DEVE_AGEFROM = cut(D_EVENT$DEVE_AGEFROM, c(0,5,10,20,30,40,65)) # onmiddelijk factoren van gemaakt
summary(D_EVENT$CAT_DEVE_AGEFROM)

# 22 
D_EVENT$CAT_DEVE_AGEFROM = cut(D_EVENT$DEVE_AGEFROM, c(-1,5,10,20,30,40,65)) # onmiddelijk factoren van gemaakt
summary(D_EVENT$CAT_DEVE_AGEFROM)

# 23 
mean(D_EVENT$DEVE_PRICE[D_EVENT$CAT_DEVE_AGEFROM == "(-1,5]"], na.rm = TRUE)
mean(D_EVENT$DEVE_PRICE[D_EVENT$CAT_DEVE_AGEFROM == "(5,10]"], na.rm = TRUE)
mean(D_EVENT$DEVE_PRICE[D_EVENT$CAT_DEVE_AGEFROM == "(10,20]"], na.rm = TRUE)
mean(D_EVENT$DEVE_PRICE[D_EVENT$CAT_DEVE_AGEFROM == "(20,30]"], na.rm = TRUE)
mean(D_EVENT$DEVE_PRICE[D_EVENT$CAT_DEVE_AGEFROM == "(30,40]"], na.rm = TRUE)
mean(D_EVENT$DEVE_PRICE[D_EVENT$CAT_DEVE_AGEFROM == "(40,65]"], na.rm = TRUE)

# 24
D_EVENT$CAT_LON_LAT = ifelse(D_EVENT$DEVE_LONGITUDE <= 4.373 & D_EVENT$DEVE_LATITUDE <= 50.98, "A",
                               ifelse(D_EVENT$DEVE_LONGITUDE <= 4.373 & D_EVENT$DEVE_LATITUDE > 50.98, "B",
                                      ifelse(D_EVENT$DEVE_LONGITUDE > 4.373 & D_EVENT$DEVE_LATITUDE <= 50.98, "C", "D")))
D_EVENT$CAT_LON_LAT = factor(D_EVENT$CAT_LON_LAT)
summary(D_EVENT$CAT_LON_LAT)

# 25 
test = D_EVENT
test = test[!is.na(test$DEVE_PRICE),]
test = test[!is.na(test$CAT_LON_LAT),]
mean(test$DEVE_PRICE[test$CAT_LON_LAT == "A"])

# 26 
mean(test$DEVE_PRICE[(test$CAT_LON_LAT == "A") | (test$CAT_LON_LAT == "C")])

# 27 
summary(D_EVENT$DEVE_LONGITUDE[D_EVENT$DEVE_CITY == 'Gent']) 

# 28
D_EVENT$DEVE_LONGITUDE = ifelse(!is.na(D_EVENT$DEVE_LONGITUDE), D_EVENT$DEVE_LONGITUDE , 3.7167 )
D_EVENT$DEVE_LATITUDE = ifelse(!is.na(D_EVENT$DEVE_LATITUDE), D_EVENT$DEVE_LATITUDE , 51.05 )
summary(D_EVENT$DEVE_LONGITUDE[D_EVENT$DEVE_CITY == 'Gent']) 


```