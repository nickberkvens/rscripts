---
title: "MPM_Analyse_28_02_18"
author: "Nick"
date: "2019 M02 26"
output:
  html_document: default
---

## packages
installeer packages

```{r setup, include=FALSE}
library(DBI)
library(odbc)
library(lubridate)
library(dplyr)
library(data.table)
library(ggplot2)
library(RMySQL)
library(tidyr)
library(maps)
library(ggmap)
library(mapdata)
library(tidyverse)
library(leaflet)
library(ggrepel)
library(RPostgres)
library(RODBC)
# library(vioplot)

# dir = getwd()
gc()
today = as.Date(Sys.time())
```

## files

lees/extraheer data van 4 tabellen in DWH (VISIT, MUSEUM, SUBSCRIPTION and PASSHOLDER)

```{r files, echo=FALSE}
library(bigrquery)
# Use your project ID here
project_id <- "reporting-224812" # put your project ID here
#  query
sql_string <- "SELECT * FROM MPM_POSTGRES_DWH.MPM_VISIT "
# Execute the query and store the result
VISIT <- query_exec(sql_string, project = project_id, useLegacySql = FALSE, max_pages = Inf )

sql_string <- "SELECT * FROM MPM_POSTGRES_DWH.MPM_MUSEUM "
# Execute the query and store the result
MUSEUM <- query_exec(sql_string, project = project_id, useLegacySql = FALSE, max_pages = Inf)

sql_string <- "SELECT * FROM MPM_POSTGRES_DWH.MPM_SUBSCRIPTION "
# Execute the query and store the result
SUBSCRIPTION <- query_exec(sql_string, project = project_id, useLegacySql = FALSE, max_pages = Inf)

sql_string <- "SELECT * FROM MPM_POSTGRES_DWH.MPM_PASSHOLDER "
# Execute the query and store the result
PASSHOLDER <- query_exec(sql_string, project = project_id, useLegacySql = FALSE, max_pages = Inf)

sql_string <- "SELECT * FROM MPM_POSTGRES_DWH.MPM_ORDERS_VIEW_BI "
# Execute the query and store the result
ORDERS_VIEW <- query_exec(sql_string, project = project_id, useLegacySql = FALSE, max_pages = Inf)


csvFileName ="/Users/nickberkvens/src/rscripts/Extracted_files/MPM_VISIT.csv"
fwrite(VISIT, file = csvFileName, row.names=F, dateTimeAs = "write.csv")
gc()

csvFileName = "/Users/nickberkvens/src/rscripts/Extracted_files/MPM_MUSEUM.csv"
fwrite(MUSEUM, file = csvFileName, row.names=F, dateTimeAs = "write.csv")
gc()

csvFileName = "/Users/nickberkvens/src/rscripts/Extracted_files/MPM_SUBSCRIPTION.csv"
fwrite(SUBSCRIPTION, file = csvFileName, row.names=F, dateTimeAs = "write.csv")
gc()

csvFileName = "/Users/nickberkvens/src/rscripts/Extracted_files/MPM_PASSHOLDER.csv"
fwrite(PASSHOLDER, file = csvFileName, row.names=F, dateTimeAs = "write.csv")
gc()

csvFileName = "/Users/nickberkvens/src/rscripts/Extracted_files/MPM_ORDERS_VIEW.csv"
fwrite(ORDERS_VIEW, file = csvFileName, row.names=F, dateTimeAs = "write.csv")
gc()


csvFileName = "/Users/nickberkvens/src/rscripts/Extracted_files/MPM_VISIT.csv"
VISIT = fread(file=csvFileName, sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
gc()

csvFileName = "/Users/nickberkvens/src/rscripts/Extracted_files/MPM_MUSEUM.csv"
MUSEUM = fread(file=csvFileName, sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
gc()

csvFileName = "/Users/nickberkvens/src/rscripts/Extracted_files/MPM_SUBSCRIPTION.csv"
SUBSCRIPTION = fread(file=csvFileName, sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
gc()

csvFileName = "/Users/nickberkvens/src/rscripts/Extracted_files/MPM_PASSHOLDER.csv"
PASSHOLDER = fread(file=csvFileName, sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
gc()

csvFileName = "/Users/nickberkvens/src/rscripts/Extracted_files/MPM_ORDERS_VIEW.csv"
ORDERS_VIEW = fread(file=csvFileName, sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
gc()
```

## etl

selecteer en herbenoem bepaalde kolommen 

```{r files2}
PASSHOLDER = PASSHOLDER[,c("id",
                           "uuid",
                           "creationdate",
                           "modifieddate",
                           "uitid",
                           "firstname",
                           "lastname",
                           "streetaddress",
                           "postalcode",
                           "addresslocality",
                           "country",
                           "birthdate",
                           "language",
                           # "email",
                           # "picture_id",
                           "state",
                           "gender",
                           "optindate"
                           # ,
                           # "subscribedtomonthlynewsletter",
                           # "subscribedtopartneremails"
                           # ,
                           # "createdbyuitid",
                           # "modifiedbyuitCid"
                           )]
names(PASSHOLDER)[1] = "passholder_id"
names(PASSHOLDER)[2] = "passholder_uuid"
names(PASSHOLDER)[3] = "passholder_creationdate"
names(PASSHOLDER)[4] = "passholder_modifieddate"

SUBSCRIPTION = SUBSCRIPTION[,c("id",
                               "uuid",
                               "creationdate",
                               "modifieddate",
                               "fromdate",
                               "todate",
                               "order_id",
                               "socialtarifftype",
                               "socialtariffidentifier",
                               "passholder_id",
                               "sequence",
                               "paidprice"
                               # ,
                               # "createdbyuitid",
                               # "modifiedbyuitid"
                               )]
names(SUBSCRIPTION)[1] = "subscription_id"
names(SUBSCRIPTION)[2] = "subscription_uuid"
names(SUBSCRIPTION)[3] = "subscription_creationdate"
names(SUBSCRIPTION)[4] = "subscription_modifieddate"

VISIT = VISIT[,c("id",
                 "uuid",
                 "creationdate",
                 "modifieddate",
                 "averageentrancefee",
                 "subscription_id",
                 "museum_id",
                 # "createdbyuitid",
                 # "modifiedbyuitid",
                 "visitdate",
                 "withretribution",
                 "correct_subscription_id"
                 )]
names(VISIT)[1] = "visit_id"
names(VISIT)[2] = "visit_uuid"
names(VISIT)[3] = "visit_creationdate"
names(VISIT)[4] = "visit_modifieddate"


MUSEUM = MUSEUM[,c("id",
                   "uuid",
                   "creationdate",
                   "modifieddate",
                   "name_nl",
                   "name_fr",
                   # "shortdescription_nl",
                   # "shortdescription_fr",
                   # "description_nl",
                   # "description_fr",
                   "streetaddress_nl",
                   "streetaddress_fr",
                   "postalcode_nl",
                   "postalcode_fr",
                   "addresslocality_nl",
                   "addresslocality_fr",
                   "openinghours_nl",
                   "openinghours_fr",
                   # "website",
                   # "phonenumber",
                   # "email",
                   # "averageentrancefee",
                   # "coverphoto_id",
                   "country"
                   # ,
                   # "copyrightinfo",
                   # "eventsearchquery",
                   # "createdbyuitid",
                   # "modifiedbyuitid",
                   # "brusselscardmuseumid",
                   # "boblegalentityidentifier",
                   # "boblocationentityidentifier"
                   )]
names(MUSEUM)[1] = "museum_id"
names(MUSEUM)[2] = "museum_uuid"
names(MUSEUM)[3] = "museum_creationdate"
names(MUSEUM)[4] = "museum_modifieddate"


ORDERS_VIEW = ORDERS_VIEW[,c("id",
                            "uuid",
                            "creationdate",
                            "modifieddate",
                            # "totalprice",
                            # "museum_id",
                            "numberofsubscriptions",
                            "orderer_id",
                            "ordertype"
                            # ,
                            # "channel"
                            # ,
                            # "createdbyuitid",
                            # "modifiedbyuitid",
                            # "molliepaymentid",
                            # "startprocessingdate",
                            # "supportreason",
                            # "externalpartner_id",
                            # "invoicenumberinmonth",
                            # "invoicenumber",
                            # "ordernumber",
                            # "museumnaam_nl",
                            # "museumnaam_fr",
                            # "state",
                            # "Invoicenumber",
                            # "externalpartnername",
                            # "delivery_postalcode"
                            )]
names(ORDERS_VIEW)[1] = "orders_view_id"
names(ORDERS_VIEW)[2] = "orders_view_uuid"
names(ORDERS_VIEW)[3] = "orders_view_creationdate"
names(ORDERS_VIEW)[4] = "orders_view_modifieddate"

```


## datashape
aantal rijen van tabellen pashouder, subscription, visits, orders

```{r shape1}
length(unique(PASSHOLDER$passholder_id))
length(unique(SUBSCRIPTION$subscription_id))
length(unique(VISIT$visit_id))
```

## combineren
combineer dataset en verwijder abonnementen die nog niet geactiveerd zijn (data$fromdate != "")  

datum variabelen transformeren naar correcte "vorm"

```{r join}
data = merge(SUBSCRIPTION, VISIT, by.x = "subscription_id", by.y = "VISIT_subscription_id", all.x = TRUE)
data = merge(data, PASSHOLDER, by.x = "SUBSCRIPTION_passholder_id", by.y = "passholder_id", all.x = TRUE)

data = data[!is.na(data$SUBSCRIPTION_passholder_id),]
data = data[!is.na(data$SUBSCRIPTION_fromdate),]
# data = data[data$SUBSCRIPTION_fromdate != "",] # if "" then subscription not yet activated

data$visitdate_date = as.Date(data$VISIT_visitdate)
data$visitdate_time = strftime(data$VISIT_visitdate, format="%H:%M:%S")
data$visitdatetime = as.POSIXct(data$VISIT_visitdate)
data$fromdate = as.Date(data$SUBSCRIPTION_fromdate)
data$todate = as.Date(data$SUBSCRIPTION_todate)
```

## visits in time
aantal visits ifv tijd visualiseren (dump van 5255 visits niet waarneembaar?)


```{r visits_time}
ggplot(data , aes(x = visitdate_date)) +
  geom_histogram() + 
  xlab("tijd bezoek") +
  ylab("aantal bezoeken") +
  theme_bw() +
  ggtitle("aantal bezoeken in de tijd")
```


## datashape
verwijder verlengde abonnementen (verwijderen om duplicaten van pashouder te vermijden)  

aantal unieke pashouders weergeven


```{r shape2}
data = data[data$fromdate < today, ]
length(unique(data$SUBSCRIPTION_passholder_id))
```

## nieuwe velden
* bereken voor elk bezoek van elke individu:
  + AGE_SUBSCR en AGE_SUBSCR_CEIL = totale activatieduur van abonnement (in maanden)
  + MONTH_IN_SUBSCR = maand in abonnement tijdens dewelke bezoek gebeurde
  + NUM_VISIT = aantal bezoeken in totaal voor pashouder
  + NUM_VISIT_MONTH = totaal aantal bezoeken in die maand (indien er een bezoek gebeurde in die maand door pashouder)
  + FREQ_VISIT = frequentie van bezoeken per maand gedurende activatieduur van abonnement (# visits/maanden)

```{r features}
data = data %>%
  mutate(AGE_SUBSCR_CEIL = ceiling(as.numeric((today - fromdate)/30.45)), AGE_SUBSCR = as.numeric((today - fromdate)/30.45))

data = data %>%
  mutate(MONTH_IN_SUBSCR = ceiling(as.numeric(visitdate_date - fromdate)/30.45))

data$AGE_SUBSCR = ifelse(data$AGE_SUBSCR == 0, 0, data$AGE_SUBSCR)
data$AGE_SUBSCR_CEIL = ifelse(data$AGE_SUBSCR_CEIL == 0, 1, data$AGE_SUBSCR_CEIL)
data$MONTH_IN_SUBSCR = ifelse(data$MONTH_IN_SUBSCR == 0, 1, data$MONTH_IN_SUBSCR)

data = data %>%
  group_by(subscription_id) %>%
  mutate(NUM_VISIT = n())

data = data %>%
  group_by(subscription_id, MONTH_IN_SUBSCR) %>%
  mutate(NUM_VISIT_MONTH = n())

data$NUM_VISIT = ifelse(is.na(data$MONTH_IN_SUBSCR), 0, data$NUM_VISIT)
data$NUM_VISIT_MONTH = ifelse(is.na(data$MONTH_IN_SUBSCR), 0, data$NUM_VISIT_MONTH)
data$MONTH_IN_SUBSCR = ifelse(is.na(data$MONTH_IN_SUBSCR), data$AGE_SUBSCR_CEIL, data$MONTH_IN_SUBSCR)
data$FREQ_VISIT = data$NUM_VISIT/data$AGE_SUBSCR
```


## histogram visits
verdeling aantal bezoeken per bezoeker

```{r hist3}
VISIT_2 = data[,c("subscription_id", "FREQ_VISIT","AGE_SUBSCR_CEIL")]
print(nrow(VISIT_2))
setDT(VISIT_2)
VISIT_2 = unique(VISIT_2)
print(nrow(VISIT_2))
VISIT_2$FREQ_VISIT = ceiling(VISIT_2$FREQ_VISIT)

VISIT_2$AGE_SUBSCR_CEIL = factor(VISIT_2$AGE_SUBSCR_CEIL)
VISIT_2$AGE_SUBSCR_CAT = ifelse(VISIT_2$AGE_SUBSCR_CEIL %in% c(1,2,3), "1-3",
                                ifelse(VISIT_2$AGE_SUBSCR_CEIL %in% c(4,5,6), "4-6",
                                       ifelse(VISIT_2$AGE_SUBSCR_CEIL %in% c(7,8,9), "7-9",
                                              ifelse(VISIT_2$AGE_SUBSCR_CEIL %in% c(10,11,12), "10-12", "13"))))

VISIT_2$AGE_SUBSCR_CAT <- factor(VISIT_2$AGE_SUBSCR_CAT, levels=c("1-3", "4-6", "7-9","10-12","13"), labels=c("1-3", "4-6", "7-9","10-12","13"))


ggplot(VISIT_2) +
  geom_bar(aes(x = FREQ_VISIT)) + 
  xlim(0,10) +
  scale_fill_brewer(palette="Set1", name = "abonementsduur (maand)") +
  xlab("aantal bezoeken per maand (naar boven afgerond)") +
  ylab("aantal bezoekers") +
  theme_bw() +
  ggtitle("verdeling van aantal bezoeken per maand") +
  facet_wrap(~AGE_SUBSCR_CAT)

ggplot(VISIT_2) +
  geom_bar(aes(x = FREQ_VISIT, fill = AGE_SUBSCR_CAT)) + 
  xlim(0,15) +
  scale_fill_brewer(palette="Set1", name = "abonementsduur (maand)") +
  xlab("aantal bezoeken per maand (naar boven afgerond)") +
  ylab("aantal bezoekers") +
  theme_bw() +
  ggtitle("verdeling van aantal bezoeken per maand")

ggplot(VISIT_2) +
  geom_bar(aes(x = FREQ_VISIT, fill = AGE_SUBSCR_CAT),position="fill") + 
  xlim(0,10) +
  scale_fill_brewer(palette="Set1", name = "abonementsduur (maand)") +
  xlab("aantal bezoeken per maand (naar boven afgerond)") +
  ylab("% verdeling per waarde aantal bezoeken") +
  theme_bw() +
  ggtitle("verdeling van aantal bezoeken per maand")

```
## 


```{r Ellen1}
# bijkomende vragen Ellen

# VISIT_2 = data[,c("subscription_id","AGE_SUBSCR_CEIL","NUM_VISIT" )]
# setDT(VISIT_2)
# VISIT_3 = unique(VISIT_2)
# nrow(VISIT_3[VISIT_3$AGE_SUBSCR_CEIL == 10,])
# nrow(VISIT_3[VISIT_3$AGE_SUBSCR_CEIL == 9,])
# nrow(VISIT_3[VISIT_3$AGE_SUBSCR_CEIL == 8,])
# nrow(VISIT_3[VISIT_3$AGE_SUBSCR_CEIL == 7,])
# nrow(VISIT_3[VISIT_3$AGE_SUBSCR_CEIL == 6,])
# nrow(VISIT_3[VISIT_3$AGE_SUBSCR_CEIL == 5,])
# nrow(VISIT_3[VISIT_3$AGE_SUBSCR_CEIL == 4,])
# nrow(VISIT_3[VISIT_3$AGE_SUBSCR_CEIL == 3,])
# nrow(VISIT_3[VISIT_3$AGE_SUBSCR_CEIL == 2,])
# nrow(VISIT_3[VISIT_3$AGE_SUBSCR_CEIL == 1,])
# 
# 
# VISIT_2 = data[,c("subscription_id","AGE_SUBSCR_CEIL","NUM_VISIT" )]
# setDT(VISIT_2)
# VISIT_2 = unique(VISIT_2)
# mean(VISIT_2$NUM_VISIT[VISIT_2$AGE_SUBSCR_CEIL == 10])
# mean(VISIT_2$NUM_VISIT[VISIT_2$AGE_SUBSCR_CEIL == 9])
# mean(VISIT_2$NUM_VISIT[VISIT_2$AGE_SUBSCR_CEIL == 8])
# mean(VISIT_2$NUM_VISIT[VISIT_2$AGE_SUBSCR_CEIL == 7])
# mean(VISIT_2$NUM_VISIT[VISIT_2$AGE_SUBSCR_CEIL == 6])
# mean(VISIT_2$NUM_VISIT[VISIT_2$AGE_SUBSCR_CEIL == 5])
# mean(VISIT_2$NUM_VISIT[VISIT_2$AGE_SUBSCR_CEIL == 4])
# mean(VISIT_2$NUM_VISIT[VISIT_2$AGE_SUBSCR_CEIL == 3])
# mean(VISIT_2$NUM_VISIT[VISIT_2$AGE_SUBSCR_CEIL == 2])
# mean(VISIT_2$NUM_VISIT[VISIT_2$AGE_SUBSCR_CEIL == 1])
# 
# VISIT_2 = data[,c("subscription_id","AGE_SUBSCR_CEIL","averageentrancefee", "visit_id")]
# setDT(VISIT_2)
# VISIT_2 = unique(VISIT_2)
# # VISIT_2 = VISIT_2 %>%
# #   group_by(AGE_SUBSCR_CEIL,subscription_id) %>%
# #   mutate(SUM_GTP_PERS = sum(averageentrancefee))
# # VISIT_2 = VISIT_2[,c("subscription_id","AGE_SUBSCR_CEIL","SUM_GTP_PERS")]
# # setDT(VISIT_2)
# # VISIT_2 = unique(VISIT_2)
# mean(VISIT_2$averageentrancefee[VISIT_2$AGE_SUBSCR_CEIL == 10],na.rm=TRUE)
# mean(VISIT_2$averageentrancefee[VISIT_2$AGE_SUBSCR_CEIL == 9],na.rm=TRUE)
# mean(VISIT_2$averageentrancefee[VISIT_2$AGE_SUBSCR_CEIL == 8],na.rm=TRUE)
# mean(VISIT_2$averageentrancefee[VISIT_2$AGE_SUBSCR_CEIL == 7],na.rm=TRUE)
# mean(VISIT_2$averageentrancefee[VISIT_2$AGE_SUBSCR_CEIL == 6],na.rm=TRUE)
# mean(VISIT_2$averageentrancefee[VISIT_2$AGE_SUBSCR_CEIL == 5],na.rm=TRUE)
# mean(VISIT_2$averageentrancefee[VISIT_2$AGE_SUBSCR_CEIL == 4],na.rm=TRUE)
# mean(VISIT_2$averageentrancefee[VISIT_2$AGE_SUBSCR_CEIL == 3],na.rm=TRUE)
# mean(VISIT_2$averageentrancefee[VISIT_2$AGE_SUBSCR_CEIL == 2],na.rm=TRUE)
# mean(VISIT_2$averageentrancefee[VISIT_2$AGE_SUBSCR_CEIL == 1],na.rm=TRUE)
# 
# VISIT_2 = data[,c("subscription_id", "NUM_VISIT", "fromdate", "AGE_SUBSCR_CEIL","AGE_SUBSCR","subscription_creationdate")]
# VISIT_2$subscription_creationdate = as.Date(VISIT_2$subscription_creationdate)
# VISIT_2 = VISIT_2[VISIT_2$subscription_creationdate == "2018-09-13", ]
# # VISIT_2 = VISIT_2[VISIT_2$subscription_creationdate <= "2018-09-18", ]
# setDT(VISIT_2)
# VISIT_2 = unique(VISIT_2)
# # VISIT_2 = VISIT_2 %>%
# #   group_by(subscription_creationdate) %>%
# #   summarise(num_registrations_date = n())
# hist(VISIT_2$AGE_SUBSCR_CEIL, labels = TRUE)
# 
# barfill <- "#4271AE"
# barlines <- "#1F3552"
# 
# ggplot(VISIT_2 , aes(x = AGE_SUBSCR_CEIL)) +
#   geom_histogram(binwidth = 1,breaks = seq(0,10,by = 1), colour = barlines, fill = barfill) +
#   xlab("aantal maanden abonnement actief") +
#   ylab("aantal abonnees VVK") +
#   scale_x_continuous(breaks = seq(0, 10, 1),
#                      limits=c(0, 10)) +
#   theme_bw() +
#   ggtitle("verdeling actievatieduur abonnees VVK ")
# 
# table(VISIT_2$AGE_SUBSCR_CEIL)
# 
# 
# 
# a_data_frame = data.frame(month = 1:9,
#                           visits = c(0.634840871,0.877721943,0.78559464,0.768844221,0.642378559,0.727805695,0.682579564,0.592127303,0.472361809))

# linearMod <- lm(visits ~ month, data=a_data_frame)  # build linear regression model on full data
# print(linearMod)


VISIT_2 = data[,c("subscription_id", "NUM_VISIT", "fromdate", "AGE_SUBSCR_CEIL","AGE_SUBSCR","todate")]
setDT(VISIT_2)
VISIT_2 = unique(VISIT_2)
VISIT_2$fromdate = as.Date(VISIT_2$fromdate)
VISIT_2$todate = as.Date(VISIT_2$todate)
VISIT_2 = VISIT_2[VISIT_2$fromdate < "2019-01-01", ]
VISIT_2$TODATE_MONTH = month(VISIT_2$todate)
VISIT_2 = VISIT_2 %>%
  group_by(TODATE_MONTH) %>%
  summarise(num_passholders_ending = n())

table(VISIT_2)




```


## histogram visits
verdeling aantal bezoeken per bezoeker

```{r hist1}
VISIT_2 = data[,c("subscription_id","NUM_VISIT", "AGE_SUBSCR", "FREQ_VISIT")]
setDT(VISIT_2)
VISIT_2 = unique(VISIT_2)

ggplot(VISIT_2 , aes(x = NUM_VISIT)) +
  geom_histogram(breaks = seq(0,45, by=0.5)) + 
  xlim(0,30) +
  # scale_fill_brewer(palette="Set1", name = "geslacht") +
  xlab("aantal bezoeken") +
  ylab("aantal bezoekers") +
  theme_bw() +
  ggtitle("aantal bezoeken per bezoeker")

```

## histogram visits freq
verdeling frequentie bezoeken per bezoeker

```{r hist2}
ggplot(VISIT_2 , aes(x = FREQ_VISIT)) +
  geom_histogram(breaks = seq(0,20, by=0.5)) + 
  xlim(0,10) +
  # scale_fill_brewer(palette="Set1", name = "geslacht") +
  xlab("aantal bezoeken/maand") +
  ylab("aantal bezoekers") +
  theme_bw() +
  ggtitle("aantal bezoeken/maand per bezoeker")

```

## maandelijkse museumbezoek gedrag
visualiseer trend in maandelijkse frequentie bezoeken rekeninghoudend met totale activatieduur van subscriptions  

probleem: als bezoeker in maaand geen bezoek doet wordt geen 0 geregistreerd en niet in gemiddelde meegenomen

```{r line}
VISIT_2 = data[,c("subscription_id","MONTH_IN_SUBSCR",  "AGE_SUBSCR_CEIL", "NUM_VISIT_MONTH")]
setDT(VISIT_2)
VISIT_2 = unique(VISIT_2)

VISIT_3 = VISIT_2[,c("subscription_id","AGE_SUBSCR_CEIL")]
setDT(VISIT_3)
VISIT_3 = unique(VISIT_3)

VISIT_4 = data.frame(subscription_id = 0, AGE_SUBSCR_CEIL = 0, MONTH_IN_SUBSCR = 0)
for (i in 1:nrow(VISIT_3)){
  a = VISIT_3$AGE_SUBSCR_CEIL[i]
  for (j in 1:VISIT_3$AGE_SUBSCR_CEIL[i]){
    a = data.frame(subscription_id = paste(VISIT_3$subscription_id[i]), AGE_SUBSCR_CEIL = paste(VISIT_3$AGE_SUBSCR_CEIL[i]), MONTH_IN_SUBSCR = paste(j))
    VISIT_4 = rbind(VISIT_4, a)
  }
}
VISIT_4 = VISIT_4[VISIT_4$subscription_id != 0,]
VISIT_4 = merge(VISIT_4, VISIT_2, by = c("subscription_id","AGE_SUBSCR_CEIL","MONTH_IN_SUBSCR"), all.x = TRUE)

VISIT_4$NUM_VISIT_MONTH = ifelse(is.na(VISIT_4$NUM_VISIT_MONTH), 0, VISIT_4$NUM_VISIT_MONTH)
VISIT_4 = VISIT_4[VISIT_4$AGE_SUBSCR_CEIL != VISIT_4$MONTH_IN_SUBSCR,]

VISIT_4 = VISIT_4 %>%
  group_by(AGE_SUBSCR_CEIL, MONTH_IN_SUBSCR) %>%
  mutate(MEAN_NUM_VISIT_MONTH = mean(NUM_VISIT_MONTH))

VISIT_4 = VISIT_4[c("AGE_SUBSCR_CEIL","MONTH_IN_SUBSCR","MEAN_NUM_VISIT_MONTH")]
setDT(VISIT_4)
VISIT_4 = unique(VISIT_4)

ggplot(VISIT_4,aes(x = MONTH_IN_SUBSCR, y = MEAN_NUM_VISIT_MONTH, group = AGE_SUBSCR_CEIL)) +
  geom_line(aes(color=factor(AGE_SUBSCR_CEIL)),size=1) +
  geom_point(aes(color=factor(AGE_SUBSCR_CEIL)),size=3) +
  xlab("maanden in abonnemment") +
  ylab("gemiddeld aantal bezoeken per maand") +
  ylim(0,3) +
  scale_colour_discrete(name = "totale activatieduur (maanden) van museumpas") +
  theme_bw() +
  ggtitle("trend in bezoeken")


ggplot(VISIT_4,aes(x = MONTH_IN_SUBSCR, y = MEAN_NUM_VISIT_MONTH, group = AGE_SUBSCR_CEIL)) +
  geom_line(aes(color=factor(AGE_SUBSCR_CEIL)),size=1) +
  geom_point(aes(color=factor(AGE_SUBSCR_CEIL)),size=3) +
  xlab("maanden in abonnemment") +
  ylab("gemiddeld aantal bezoeken per maand") +
  ylim(0,1.5) +
  scale_colour_discrete(name = "activatieduur pas (m)") +
  theme_bw() +
  ggtitle("trend in bezoeken")

fwrite(VISIT_4, file = "C:/Users/nickberkvens/Desktop/cohorten_MPM_Ellen.csv", row.names=F)
```

## actieve passen zonder bezoeken
raporteer totaal aantal gebruikers en  
aantal gebruikers met actieve pas maar zonder bezoeken

```{r act pass}
length(unique(data$passholder_id))
nrow(data[data$NUM_VISIT == 0,])
```

## visualisering businessmodel
* bereken per individu:
  + retributiekost_visit = retributiekost van elke bezoek; = averageentrancefee*0.6
  + retributiekost_visit_maand = totale retributie kost in die maand waarop bezoek gebeurde
  + maandel_retributiekost = gemiddelde maandelijkse retributiekost
  
enkel indien volledige maand van activatie van subscription verlopen is wordt data betrokken in analyse (MONTH_IN_SUBSCR != AGE_SUBSCR)

```{r businessmodel}
VISIT_2 = data
VISIT_2$retributiekost_visit = VISIT_2$averageentrancefee*0.6
# VISIT_2 = VISIT_2[VISIT_2$NUM_VISIT != 0,]
VISIT_2 = VISIT_2[ VISIT_2$MONTH_IN_SUBSCR != VISIT_2$AGE_SUBSCR_CEIL | VISIT_2$NUM_VISIT == 0,] # volledige  maand moet gepasseerd zijn

VISIT_2 = VISIT_2 %>%
  group_by(passholder_id) %>%
  mutate(maandel_retributiekost = sum(retributiekost_visit)/(AGE_SUBSCR_CEIL-1))

VISIT_2$maandel_retributiekost = ifelse(is.na(VISIT_2$maandel_retributiekost), 0, VISIT_2$maandel_retributiekost)

VISIT_2 = VISIT_2 %>%
  group_by(passholder_id) %>%
  mutate(Winst = 3.74 - maandel_retributiekost)

VISIT_2 = VISIT_2[,c("passholder_id","AGE_SUBSCR_CEIL","NUM_VISIT","maandel_retributiekost","Winst")]
setDT(VISIT_2)
VISIT_2 = unique(VISIT_2)
```

## verdeling gemiddelde maandelijkse retributiekost

```{r hist maandel_retributiekost}
ggplot(VISIT_2,aes(x=maandel_retributiekost)) +
  geom_histogram(binwidth = 0.5, colour="black", fill="white" ) +
  geom_vline(aes(xintercept=3.74), color="red", linetype="dashed", size=1)
```

## light-medium gebruikers (gem maandelijkse retributiekost < 3.74 euro)
aantal pashouders en maandelijkse inkomsten mbt retributie

```{r gain1}
nrow(VISIT_2[VISIT_2$maandel_retributiekost < 3.74,])
3.74*nrow(VISIT_2[VISIT_2$maandel_retributiekost < 3.74,]) - sum(VISIT_2$maandel_retributiekost[VISIT_2$maandel_retributiekost < 3.74])
```

## heavy gebruikers (gem maandelijkse retributiekost > 3.74 euro)
aantal pashouders en maandelijkse uitgaven mbt retributie

```{r gain2}
nrow(VISIT_2[VISIT_2$maandel_retributiekost > 3.74,])
3.74*nrow(VISIT_2[VISIT_2$maandel_retributiekost > 3.74,]) - sum(VISIT_2$maandel_retributiekost[VISIT_2$maandel_retributiekost > 3.74])
```

## ballans inkomsten-uitgaven mbt maandelijkse retributiekost vandaag

```{r ballans}
3.74*nrow(VISIT_2) - (sum(VISIT_2$maandel_retributiekost))

VISIT_2 = VISIT_2 %>%
  group_by(passholder_id) %>%
  mutate(Gain_class = ifelse(Winst >= 0, "gain", "loss"))

VISIT_2 = VISIT_2 %>%
  group_by(Gain_class) %>%
  mutate(Gain_sum = sum(Winst))

VISIT_3 = VISIT_2[,c("Gain_class","Gain_sum")]
setDT(VISIT_3)
VISIT_3 = unique(VISIT_3)

ggplot(VISIT_3,aes(x= Gain_class, y=Gain_sum)) +
  geom_col() +
  scale_y_continuous(breaks = scales::pretty_breaks(n = 10),labels = scales::number_format(accuracy = 1,
                                 decimal.mark = ',')) +
  labs(x = "retributie inkomsten light-medium vs heavy users", y = "bedrag in euros")
  
```


## ratio light-medium vs. heavy users

ratio aantal light-medium users voor iedere heavy users
```{r ratio1}
a = (3.74*nrow(VISIT_2[VISIT_2$maandel_retributiekost < 3.74,]) - sum(VISIT_2$maandel_retributiekost[VISIT_2$maandel_retributiekost < 3.74]))/nrow(VISIT_2[VISIT_2$maandel_retributiekost < 3.74,])

b = (3.74*nrow(VISIT_2[VISIT_2$maandel_retributiekost > 3.74,]) - sum(VISIT_2$maandel_retributiekost[VISIT_2$maandel_retributiekost > 3.74]))/nrow(VISIT_2[VISIT_2$maandel_retributiekost > 3.74,])

c = (-1)*b/a
c

```

## ratio light-medium vs. heavy users

aantal nieuwe light-medium users nodig voor balans mbt retributie
```{r ratio2}
nrow(VISIT_2[VISIT_2$maandel_retributiekost > 3.74,])*c - nrow(VISIT_2[VISIT_2$maandel_retributiekost < 3.74,])
  
```

## characteristics light-medium vs. heavy users

maak 2 groepen Light-Medium vs.Heavy, aantal:
```{r char1}
VISIT_2 = data
VISIT_2$retributiekost_visit = VISIT_2$averageentrancefee*0.6
VISIT_2 = VISIT_2 %>%
  group_by(passholder_id) %>%
  mutate(maandel_retributiekost = sum(retributiekost_visit)/(AGE_SUBSCR_CEIL-1))
VISIT_2$maandel_retributiekost = ifelse(is.na(VISIT_2$maandel_retributiekost), 0, VISIT_2$maandel_retributiekost)
VISIT_2 = VISIT_2 %>%
  group_by(passholder_id) %>%
  mutate(Winst = 3.74 - maandel_retributiekost)
VISIT_2 = VISIT_2[,c("passholder_id","AGE_SUBSCR_CEIL","NUM_VISIT","maandel_retributiekost","Winst")]
setDT(VISIT_2)
VISIT_2 = unique(VISIT_2)


VISIT_2$Type_user = ifelse(VISIT_2$maandel_retributiekost > 3.74, "Heavy", "Light-Medium")
VISIT_2$Type_user = factor(VISIT_2$Type_user)
summary(VISIT_2$Type_user)

VISIT_3 = data[,c("passholder_id","socialtarifftype","socialtariffidentifier","streetaddress",
                  "postalcode","addresslocality","country","birthdate","language","state","gender")]

setDT(VISIT_3)
VISIT_3 = unique(VISIT_3)

VISIT_2 = merge(VISIT_2,VISIT_3, by.x = "passholder_id", by.y = "passholder_id", all.x = TRUE)

VISIT_2 = VISIT_2[!is.na(VISIT_2$language),]


age <- function(dob, age.day = today(), units = "years", floor = TRUE) {
  calc.age = interval(dob, age.day) / duration(num = 1, units = units)
  if (floor) return(as.integer(floor(calc.age)))
  return(calc.age)
}

VISIT_2$birthdate = as.Date(VISIT_2$birthdate)
VISIT_2 = VISIT_2 %>%
  mutate (Age = age(birthdate))

VISIT_2$Age_category <- cut(VISIT_2$Age, 
                   breaks=c(0,10,20,25,30,35,40,45,50,55,60,65,70,75,80,150), 
                   labels=c("0-9", "10-19","20-24","25-29","30-34","35-39","40-44","45-49","50-54","55-59","60-64","65-69"
                            ,"70-74","75-79","+80"))

csvFileName = paste(dir,"/R_Scripts/Extracted_files/be_postal_codes.csv", sep="")
postal_codes = fread(file=csvFileName, sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
gc()

names(postal_codes)[1] = "Postal_code"
names(postal_codes)[2] = "Place_name"
names(postal_codes)[3] = "Place_name_2"
names(postal_codes)[5] = "State_abbreviation"

csvFileName = paste(dir,"/R_Scripts/Extracted_files/Wettelijke_bevolking_per_gemeente_2017.csv", sep="")
postal_dens = fread(file=csvFileName, sep=",", header = T, stringsAsFactors = T, na.strings = "NULL")
gc()

VISIT_2 = merge(VISIT_2, postal_codes, by.x = "postalcode", by.y = "Postal_code", all.x = TRUE)
VISIT_2 = merge(VISIT_2, postal_dens, by.x = "Place_name", by.y = "Gemeente" , all.x = TRUE)


VISIT_2$Stad_grootte <- cut(VISIT_2$Aantal_inwoners, 
                   breaks=c(0,20000,40000,100000,120000,650000), 
                   labels=c("0-20K","20-40K","40-100K","100-120K","+120K"))

VISIT_2 = VISIT_2[!is.na(VISIT_2$Stad_grootte),]
VISIT_2$Stad_grootte = factor(VISIT_2$Stad_grootte)
VISIT_2 = VISIT_2[VISIT_2$City != "Hainaut Mons",]
VISIT_2$City = factor(VISIT_2$City)
VISIT_2$Type_user = factor(VISIT_2$Type_user, levels = c("Light-Medium" , "Heavy"))

######### language

table(VISIT_2$language,VISIT_2$Type_user)
ggplot(VISIT_2, aes(x = language, group = Type_user ))+
  geom_bar(aes(fill = factor(Type_user)), stat="count") +
  ylab("aantal") +
  ggtitle("ligh-medium vs heavy profielen ifv 'language'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = language, group = Type_user ))+
  geom_bar(aes(y = ..prop.., fill = factor(Type_user)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'language'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = Type_user, group = language ))+
  geom_bar(aes(y = ..prop.., fill = factor(language)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'language'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~language)

######### gender

table(VISIT_2$gender,VISIT_2$Type_user)
ggplot(VISIT_2, aes(x = gender, group = Type_user ))+
  geom_bar(aes(fill = factor(Type_user)), stat="count") +
  ylab("aantal") +
  ggtitle("ligh-medium vs heavy profielen ifv 'gender'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = gender, group = Type_user ))+
  geom_bar(aes(y = ..prop.., fill = factor(Type_user)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'gender'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = Type_user , group = gender ))+
  geom_bar(aes(y = ..prop.., fill = factor(gender)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'gender'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~gender)

############## Age_category

table(VISIT_2$Age_category,VISIT_2$Type_user)
ggplot(VISIT_2, aes(x = Age_category, group = Type_user ))+
  geom_bar(aes(fill = factor(Type_user)), stat="count") + 
  ylab("aantal") +
  ggtitle("ligh-medium vs heavy profielen ifv 'Age_category'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = Age_category, group = Type_user ))+
  geom_bar(aes(y = ..prop.., fill = factor(Type_user)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'Age_category'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = Type_user , group = Age_category ))+
  geom_bar(aes(y = ..prop.., fill = factor(Age_category)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'Age_category'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Age_category)

######## socialtarifftype

table(VISIT_2$socialtarifftype,VISIT_2$Type_user)
ggplot(VISIT_2, aes(x = socialtarifftype, group = Type_user ))+
  geom_bar(aes(fill = factor(Type_user)), stat="count") + 
  ylab("aantal") +
  ggtitle("ligh-medium vs heavy profielen ifv 'socialtarifftype'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = socialtarifftype, group = Type_user ))+
  geom_bar(aes(y = ..prop.., fill = factor(Type_user)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'socialtarifftype'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = Type_user , group = socialtarifftype ))+
  geom_bar(aes(y = ..prop.., fill = factor(socialtarifftype)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'socialtarifftype'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~socialtarifftype)


######## State

table(VISIT_2$State,VISIT_2$Type_user)
ggplot(VISIT_2, aes(x = State, group = Type_user ))+
  geom_bar(aes(fill = factor(Type_user)), stat="count") + 
  ylab("aantal") +
  ggtitle("ligh-medium vs heavy profielen ifv 'State'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = State, group = Type_user ))+
  geom_bar(aes(y = ..prop.., fill = factor(Type_user)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'State'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = Type_user , group = State ))+
  geom_bar(aes(y = ..prop.., fill = factor(State)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'State'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~State)

######## City

table(VISIT_2$City,VISIT_2$Type_user)
ggplot(VISIT_2, aes(x = City, group = Type_user ))+
  geom_bar(aes(fill = factor(Type_user)), stat="count") + 
  ylab("aantal") +
  ggtitle("ligh-medium vs heavy profielen ifv 'Province'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = City, group = Type_user ))+
  geom_bar(aes(y = ..prop.., fill = factor(Type_user)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'Province'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = Type_user , group = City ))+
  geom_bar(aes(y = ..prop.., fill = factor(City)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'Province'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~City)

######## Stad_grootte

table(VISIT_2$Stad_grootte,VISIT_2$Type_user)
ggplot(VISIT_2, aes(x = Stad_grootte, group = Type_user ))+
  geom_bar(aes(fill = factor(Type_user)), stat="count") + 
  ylab("aantal") +
  ggtitle("ligh-medium vs heavy profielen ifv 'Stad_grootte'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = Stad_grootte, group = Type_user ))+
  geom_bar(aes(y = ..prop.., fill = factor(Type_user)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'Stad_grootte'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = Type_user , group = Stad_grootte ))+
  geom_bar(aes(y = ..prop.., fill = factor(Stad_grootte)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ggtitle("ligh-medium vs heavy profielen ifv 'Stad_grootte'") +
  ylab("percentages") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Stad_grootte)

######## AGE_SUBSCR_CEIL

table(VISIT_2$AGE_SUBSCR_CEIL ,VISIT_2$Type_user)
ggplot(VISIT_2, aes(x = AGE_SUBSCR_CEIL, group = Type_user ))+
  geom_bar(aes(fill = factor(Type_user)), stat="count") + 
  ylab("aantal") +
  ggtitle("ligh-medium vs heavy profielen ifv 'Activatieduur abonnement'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = AGE_SUBSCR_CEIL, group = Type_user ))+
  geom_bar(aes(y = ..prop.., fill = factor(Type_user)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ylab("percentages") +
  ggtitle("ligh-medium vs heavy profielen ifv 'Activatieduur abonnement'") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~Type_user)

ggplot(VISIT_2, aes(x = Type_user , group = AGE_SUBSCR_CEIL ))+
  geom_bar(aes(y = ..prop.., fill = factor(AGE_SUBSCR_CEIL)), stat="count") + 
  scale_y_continuous(labels=scales::percent) +
  ggtitle("ligh-medium vs heavy profielen ifv 'Activatieduur abonnement'") +
  ylab("percentages") +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90), legend.position="none" ) +
  facet_grid(~AGE_SUBSCR_CEIL)

```



